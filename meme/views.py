from rest_framework import viewsets
from rest_framework.response import Response
from django.db.models import Q
from rest_framework import filters

from .serializers import MemeSerializer
from .models import Meme
# Create your views here.

class MemeViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = MemeSerializer
    queryset = Meme.objects.filter(~Q(hidden=1))
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name','tag')