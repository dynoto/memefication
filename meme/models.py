from django.db import models
from sorl.thumbnail import ImageField
import uuid, os

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('images', filename)
# Create your models here.
class Meme(models.Model):

    class Meta:
        ordering = ['id']

    name        = models.CharField(max_length=128)
    tag         = models.TextField()
    image       = ImageField(upload_to=get_file_path)
    hidden      = models.BooleanField(default=False)