from django.contrib import admin
from meme.models import Meme
from sorl.thumbnail.admin import AdminImageMixin


# Register your models here.
@admin.register(Meme)
class MemeAdmin(AdminImageMixin,admin.ModelAdmin):
    list_display = ('id','name','tag','image','hidden')