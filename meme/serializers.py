from rest_framework import serializers
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField
from .models import Meme

class MemeSerializer(serializers.ModelSerializer):
    thumbnail = HyperlinkedSorlImageField(
        '256x256',
        options={"crop": "center"},
        source='image',
        read_only=True
    )

    class Meta:
        model = Meme
        fields = ('id','name','tag','image','thumbnail')