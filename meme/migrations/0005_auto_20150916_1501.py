# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import meme.models
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('meme', '0004_remove_meme_thumbnail'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='meme',
            options={'ordering': ['id']},
        ),
        migrations.AlterField(
            model_name='meme',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to=meme.models.get_file_path),
        ),
    ]
