# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meme', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='meme',
            name='hidden',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='meme',
            name='thumbnail',
            field=models.ImageField(upload_to='thumb', default='', editable=False),
            preserve_default=False,
        ),
    ]
